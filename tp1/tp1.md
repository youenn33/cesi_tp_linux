# TP1 – LE MÉEC Youenn

## Préliminaires
````
 |[youenn@localhost ~]$ yum install -y vim
````

* Désactivation de SELinux :
````
[youenn@localhost ~]$ sudo setenforce 0
[sudo] password for youenn:
[youenn@localhost ~]$ sudo sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config
````
### I. Utilisateurs
#### Création et configuration

* Ajout d’un utilisateur :    
````
[youenn@localhost ~]$ sudo useradd youenn2
````
On lui défini un mot de passe avec la commande passwd youenn2

le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans /home : 
````
[youenn@localhost ~]$ cd /home
[youenn@localhost home]$ ls
youenn  youenn2
````
* crée groupe admins : 
````
[youenn@localhost home]$ sudo groupadd admins
````
 * on ajoute une ligne basique qui permet au groupe d'avoir tous les droits : 
 ````
[youenn@localhost home]$ sudo visudo

Allows people in group wheel to run all commands
%wheel  ALL=(ALL)       ALL
admins  ALL=(ALL)       ALL
````


* ajouter notre utilisateur a notre groupe admins :
````
[youenn@localhost home]$ sudo usermod -aG admins youenn2
[youenn@localhost home]$ groups youenn2
youenn2 : youenn2 admins
````

 ### Génération d’une clé ssh :
````
PS C:\Users\YOUENN> ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\YOUENN/.ssh/id_rsa):
Created directory 'C:\Users\YOUENN/.ssh'.
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\YOUENN/.ssh/id_rsa.
Your public key has been saved in C:\Users\YOUENN/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:EHYkMakuTJWi++ZCE5PL6RfeJpWjysqdfZJFCVRNNEU youenn@PC-Youenn-Gamer
The key's randomart image is:
+---[RSA 4096]----+
|    .oB=*+oE     |
|  . oo.= ..      |
| ..o .o .        |
|.+. .  +         |
|.+=.  o S        |
|.*o..+ .         |
|o.o.= +          |
|+.+=o= .         |
|oB+oo.o          |
+----[SHA256]-----+
````

- On copie bien notre clé publique que l’ont va mettre dans notre machine
````
PS C:\Users\YOUENN\.ssh> cat .\id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCtv7DaUb7EfXHgR5letXVlqPQiIf7Uf8DFunJ+lHs+Prwt6GhuoNmkJNKofP6HSlm4q1f5I6nw4VY9FbM0OMw/YV4zaI2D5XverW1qVDhhW2Ori7FDul8m99U9V9q8B/qCtqA5yXh8dV9QkL4fMnyF7Rlodi+zRcRPPM1MG1G8R43xhZByi/V/6TaB7wNtHwE99LeY3rfYFi74f4GIYspITc61+PbiKn7QPBt2be2PfzEYP0acdcCUbJludg6jwoDdAYYk1tlhgeN/bRA8b6XcY21EobNUwQ7g6lyk1QBIShbIOK4HOc1qHRdc+IGoUxV4YKXgvYEjfe8UXM7f9zJujZeNLh5nQE4Naolt3oDgF1UIDgEJlCbbf7w/Tk7UKQ/KqBOeCK7lgQw8mKgVGHaeh6+6qO/yOkvjNyN5uVyFXvlkcS9mvpwKVnm72geFQECTf7KNUXxiHrgddlz2nTwD5sYpkK4gN4t9rkueVQGIb5Qtrfou6Hoq/3GCKA0gii+oM1PWAgJ20YnzGCaVdA+/8bQjRzj2mIus264hntgXEdmKYCvrRx7GKnLBKV08VmOLWsC9S0wdhc1UBZYRMkGw5X7EikVn7Jyjbfrg0DrbTTu4boTxuNEymDykshHYC5Md8u8xWQpupiakQYCmsg+4eKmXT0ET87EoCoLmkT/JSQ== youenn@PC-Youenn-Gamer
````
* On crée notre fichier pour la clé et on ajoute les bons droit : 
````
[youenn@localhost ~]$ cd /home/youenn/.ssh
[youenn@localhost .ssh]$ touch authorized_keys
[youenn@localhost .ssh]$ vi authorized_keys
[youenn@localhost .ssh]$ chmod 600 /home/youenn/.ssh/authorized_keys
[youenn@localhost .ssh]$ chmod 700 /home/youenn/.ssh
````
## Configuration réseau
### Définir un nom de domaine à la machine :
 ````
[youenn@localhost system]$ sudo hostname TP-LINUX
[youenn@localhost system]$ hostname
TP-LINUX
````
````
[youenn@localhost system]$ sudo vim /etc/hostname
````
Et on remplace le contenu par TP-LINUX 
### Serveur DNS
````
[youenn@localhost system]$ sudo vim /etc/resolv.conf
nameserver 1.1.1.1
````
- On test : 
````
[youenn@TP-LINUX ~]$ curl -v google.com
* About to connect() to google.com port 80 (#0)
*   Trying 216.58.213.142...
* Connected to google.com (216.58.213.142) port 80 (#0)
> GET / HTTP/1.1
> User-Agent: curl/7.29.0
> Host: google.com
> Accept: */*
>
< HTTP/1.1 301 Moved Permanently
< Location: http://www.google.com/
< Content-Type: text/html; charset=UTF-8
< Date: Wed, 06 Jan 2021 12:38:43 GMT
< Expires: Fri, 05 Feb 2021 12:38:43 GMT
< Cache-Control: public, max-age=2592000
< Server: gws
...etc
````
## Partitionnement
````
[youenn@localhost ~]$ lsblk
NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda               8:0    0    8G  0 disk
├─sda1            8:1    0    1G  0 part /boot
└─sda2            8:2    0    7G  0 part
  ├─centos-root 253:0    0  6.2G  0 lvm  /
  └─centos-swap 253:1    0  820M  0 lvm  [SWAP]
sdb               8:16   0    3G  0 disk
sdc               8:32   0    3G  0 disk
sr0              11:0    1 1024M  0 rom
````
* Nous allons ajouter sdb et sdc dans lvm, pour crée un volume group :
````
[youenn@localhost ~]$ sudo vgcreate data /dev/sdb /dev/sdc
[sudo] password for youenn:
  Physical volume "/dev/sdb" successfully created.
  Physical volume "/dev/sdc" successfully created.
  Volume group "data" successfully created
````
 - On vérifie :
````
[youenn@localhost ~]$ sudo vgs
  VG     #PV #LV #SN Attr   VSize  VFree
  centos   1   2   0 wz--n- <7.00g    0
  data     2   0   0 wz--n-  5.99g 5.99g
````



* Il faut crée 3 logical volume : 
 ````
[youenn@localhost ~]$ sudo lvcreate -L 2G data -n ma_data
  Logical volume "ma_data" created.
  
[youenn@localhost ~]$ sudo lvcreate -L 2G data -n ma_data2
  Logical volume "ma_data2" created.
  
[youenn@localhost ~]$ sudo lvcreate -l 100%FREE data -n ma_data3
  Logical volume "ma_data3" created.
````
- On verif :
````
 [youenn@localhost ~]$ sudo lvs 
[sudo] password for youenn:
  LV       VG     Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root     centos -wi-ao----  <6.20g
  swap     centos -wi-ao---- 820.00m
  ma_data  data   -wi-a-----   2.00g
  ma_data2 data   -wi-a-----   2.00g
  ma_data3 data   -wi-a-----   1.99g
````
* On va monter ces partitions en ext4 :
````
[youenn@localhost ~]$ sudo mkfs -t ext4 /dev/data/ma_data
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
131072 inodes, 524288 blocks
26214 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=536870912
16 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
````
* On fait la même pour les deux autre partitions : 
````
[youenn@localhost ~]$ mkfs -t ext4 /dev/data/ma_data2
[youenn@localhost ~]$ mkfs -t ext4 /dev/data/ma_data3
````

* On va monter les partitions, pour cela on va crée nos dossier pour nos partitions :
````
[youenn@localhost home]$ sudo mkdir /mnt/part1
[youenn@localhost home]$ sudo mkdir /mnt/part2
[youenn@localhost home]$ sudo mkdir /mnt/part3

[youenn@localhost home]$ sudo mount /dev/data/ma_data /mnt/part1
[youenn@localhost home]$ sudo mount /dev/data/ma_data2 /mnt/part2
[youenn@localhost home]$ sudo mount /dev/data/ma_data3 /mnt/part3
````
- Pour que cette partition soit montée automatiquement au démarrage du système :
````
[youenn@localhost home]$ sudo vim /etc/fstab

/dev/data/ma_data /mnt/part1 ext4 defaults 0 0
/dev/data/ma_data2 /mnt/part2 ext4 defaults 0 0
/dev/data/ma_data3 /mnt/part3 ext4 defaults 0 0
````
## Gestion de services
### Interaction avec un service existant
Firewalld est bien démarré et activé :
````
[youenn@localhost home]$ systemctl status firewalld
● firewalld.service - firewalld - dynamic firewall daemon
   Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2021-01-06 09:55:12 CET; 41min ago
     Docs: man:firewalld(1)
 Main PID: 732 (firewalld)
   CGroup: /system.slice/firewalld.service
           └─732 /usr/bin/python2 -Es /usr/sbin/firewalld --nofork --nopid

Jan 06 09:55:10 localhost.localdomain systemd[1]: Starting firewalld - dynamic firewall daemon...
Jan 06 09:55:12 localhost.localdomain systemd[1]: Started firewalld - dynamic firewall daemon.
Jan 06 09:55:12 localhost.localdomain firewalld[732]: WARNING: AllowZoneDrifting is enabled. This is considered a...now.
Hint: Some lines were ellipsized, use -l to show in full.
````
 ### Création de service
 #### Unité simpliste
````
[youenn@localhost system]$ touch web.service
````
Ensuite j’ajoute dans le fichier :
````
[youenn@localhost system]$ sudo vim web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python2 -m SimpleHTTPServer 8888

[Install]
WantedBy=multi-user.target
````
- On ouvre le port 8888 : 
````
[youenn@localhost system]$ sudo firewall-cmd --add-port=8888/tcp --permanent
[sudo] password for youenn:
Success
[youenn@localhost system]$ sudo firewall-cmd --reload
success
````
- Nous devons demander à systemd de relire les fichier des conf :
````
[youenn@localhost system]$ sudo systemctl daemon-reload
````

- Non allons démarrer notre service web : 
````
[youenn@localhost system]$ sudo systemctl start web
[youenn@localhost system]$ sudo systemctl status web
● web.service
   Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-01-06 10:48:08 CET; 4s ago
 Main PID: 4052 (python2)
   CGroup: /system.slice/web.service
           └─4052 /bin/python2 -m SimpleHTTPServer 8888

Jan 06 10:48:08 localhost.localdomain systemd[1]: [/etc/systemd/system/web.service:1] Unknown section 'unit'. Ignoring.
Jan 06 10:48:08 localhost.localdomain systemd[1]: Started web.service.
Hint: Some lines were ellipsized, use -l to show in full.
[youenn@localhost system]$ sudo systemctl enable web
Created symlink from /etc/systemd/system/multi-user.target.wants/web.service to /etc/systemd/system/web.service.
````



- On vérifie :
````
C:\Users\YOUENN>curl -v 192.168.1.10:8888
* Rebuilt URL to: 192.168.1.10:8888/
*   Trying 192.168.1.10...
* TCP_NODELAY set
* Connected to 192.168.1.10 (192.168.1.10) port 8888 (#0)
> GET / HTTP/1.1
> Host: 192.168.1.10:8888
> User-Agent: curl/7.55.1
> Accept: */*
>
* HTTP 1.0, assume close after body
< HTTP/1.0 200 OK
< Server: SimpleHTTP/0.6 Python/2.7.5
< Date: Wed, 06 Jan 2021 10:29:42 GMT
< Content-type: text/html; charset=UTF-8
< Content-Length: 728
<
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
<h2>Directory listing for /</h2>
<hr>
<ul>
<li><a href="bin/">bin@</a>
<li><a href="boot/">boot/</a>
<li><a href="dev/">dev/</a>
<li><a href="etc/">etc/</a>
<li><a href="home/">home/</a>
<li><a href="lib/">lib@</a>
<li><a href="lib64/">lib64@</a>
<li><a href="media/">media/</a>
<li><a href="mnt/">mnt/</a>
<li><a href="opt/">opt/</a>
<li><a href="proc/">proc/</a>
<li><a href="root/">root/</a>
<li><a href="run/">run/</a>
<li><a href="sbin/">sbin@</a>
<li><a href="srv/">srv/</a>
<li><a href="sys/">sys/</a>
<li><a href="tmp/">tmp/</a>
<li><a href="usr/">usr/</a>
<li><a href="var/">var/</a>
</ul>
<hr>
</body>
</html>
* Closing connection 0
````


#### Modification de l'unité
````
[youenn@localhost ~]$ sudo useradd web
````
On l’ajoute dans le visudo

- On crée notre dossier : 
````
[youenn@localhost srv]$ mkdir web
[youenn@localhost srv]$ sudo vim /etc/systemd/system/web.service
````
- On y modifie : 
````
[Service]
ExecStart=/bin/python2 -m SimpleHTTPServer 8888
User=web
WorkingDirectory=/srv/web
````
- Le port 8888 écoute bien : 
````
[web@TP-LINUX ~]$ sudo ss -alpnt
[sudo] password for web:
State      Recv-Q Send-Q               Local Address:Port                              Peer Address:Port
LISTEN     0      5                                *:8888                                         *:*  
````

- On test : 
````
[web@TP-LINUX ~]$ curl localhost:8888
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN"><html>
<title>Directory listing for /</title>
<body>
<h2>Directory listing for /</h2>
<hr>
<ul>
<li><a href="bin/">bin@</a>
<li><a href="boot/">boot/</a>
<li><a href="dev/">dev/</a>
<li><a href="etc/">etc/</a>
<li><a href="home/">home/</a>
<li><a href="lib/">lib@</a>
<li><a href="lib64/">lib64@</a>
<li><a href="media/">media/</a>
<li><a href="mnt/">mnt/</a>
<li><a href="opt/">opt/</a>
<li><a href="proc/">proc/</a>
<li><a href="root/">root/</a>
<li><a href="run/">run/</a>
<li><a href="sbin/">sbin@</a>
<li><a href="srv/">srv/</a>
<li><a href="sys/">sys/</a>
<li><a href="tmp/">tmp/</a>
<li><a href="usr/">usr/</a>
<li><a href="var/">var/</a>
</ul>
<hr>
</body>
</html>
````

Nous avons bien accès à notre fichier via notre navigateur via le chemin ou on l’a mis.

