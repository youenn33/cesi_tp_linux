 # TP2 : Serveur Web

Nous avons configurer nos trois machine(conf ip et hostname).

## I.	Base de données
Sur db.tp2.cesi on installe les bons paquets : 
```
[youenn@db ~]$ sudo yum install -y mariadb-server
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: centos.crazyfrogs.org
 * extras: centos.crazyfrogs.org
 * updates: centos.crazyfrogs.org
Resolving Dependencies
--> Running transaction check
---> Package mariadb-server.x86_64 1:5.5.68-1.el7 will be installed
--> Processing Dependency: mariadb(x86-64) = 1:5.5.68-1.el7 for package: 1:mariadb-server-5.5.68-1.el7.x86_64
--> Processing Dependency: perl-DBI for package: 1:mariadb-server-5.5.68-1.el7.x86_64
--> Processing Dependency: perl-DBD-MySQL for package: 1:mariadb-server-5.5.68-1.el7.x86_64
--> Processing Dependency: perl(Data::Dumper) for package: 1:mariadb-server-5.5.68-1.el7.x86_64
--> Processing Dependency: perl(DBI) for package: 1:mariadb-server-5.5.68-1.el7.x86_64
-
-
Installed:
  mariadb-server.x86_64 1:5.5.68-1.el7

Dependency Installed:
  mariadb.x86_64 1:5.5.68-1.el7                                 perl-Compress-Raw-Bzip2.x86_64 0:2.061-3.el7
  perl-Compress-Raw-Zlib.x86_64 1:2.061-4.el7                   perl-DBD-MySQL.x86_64 0:4.023-6.el7
  perl-DBI.x86_64 0:1.627-4.el7                                 perl-Data-Dumper.x86_64 0:2.145-3.el7
  perl-IO-Compress.noarch 0:2.061-2.el7                         perl-Net-Daemon.noarch 0:0.48-5.el7
  perl-PlRPC.noarch 0:0.2020-14.el7

Complete!
```
- On va démarrer le service : 
```
[youenn@db ~]$ systemctl status mariadb
● mariadb.service - MariaDB database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
```
```
[youenn@db ~]$ systemctl start mariadb
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ===
Authentication is required to manage system services or units.
Authenticating as: youenn
Password:
==== AUTHENTICATION COMPLETE ===
```

- On vérifie que le service est actif :

````
[youenn@db ~]$ systemctl status mariadb
● mariadb.service - MariaDB database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2021-01-06 15:51:39 CET; 3s ago
  Process: 1838 ExecStartPost=/usr/libexec/mariadb-wait-ready $MAINPID (code=exited, status=0/SUCCESS)
  Process: 1755 ExecStartPre=/usr/libexec/mariadb-prepare-db-dir %n (code=exited, status=0/SUCCESS)
 Main PID: 1837 (mysqld_safe)
   CGroup: /system.slice/mariadb.service
           ├─1837 /bin/sh /usr/bin/mysqld_safe --basedir=/usr
           └─2002 /usr/libexec/mysqld --basedir=/usr --datadir=/var/lib/mysql --plugin-dir=/usr/lib64/mysql/plugin --log-er...

Jan 06 15:51:37 db.tp2.cesi mariadb-prepare-db-dir[1755]: MySQL manual for more instructions.
[…]Jan 06 15:51:39 db.tp2.cesi systemd[1]: Started MariaDB database server.
Hint: Some lines were ellipsized, use -l to show in full.
````

- Nous allons nous connecter à mysql :

````
[youenn@db etc]$ sudo mysql

````
- Ensuite nous allons créer notre base :

````
MariaDB [(none)]> create database mabase;
Query OK, 1 row affected (0.00 sec)
````
- On vérifie que notre base est bien crée : 
````
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mabase             |
| mysql              |
| performance_schema |
| test               |
+--------------------+
5 rows in set (0.00 sec)
````

- Nous allons créer un utilisateur et attribuer les droits sur la base de donnée à l’utilisateur : 
````
MariaDB [(none)]> create user 'youenn'@'10.99.99.11' identified by 'password';
MariaDB [(none)]> grant all privileges on mabase.* to 'youenn'@'10.99.99.11';
````

- On vérifie que notre utilisateur est bien dans notre base de données :
````
MariaDB [mabase]> select user from mysql.user;
+--------+
| user   |
+--------+
| youenn |
| root   |
| root   |
|        |
| root   |
|        |
| root   |
| youenn |
+--------+
````
- On peut voir à quel base notre utilisateur est associé et ses privilèges :
````
MariaDB [mabase]> SHOW GRANTS FOR 'youenn@localhost';
ERROR 1141 (42000): There is no such grant defined for user 'youenn@localhost' on host '%'
MariaDB [mabase]> SHOW GRANTS FOR youenn@localhost;
+---------------------------------------------------------------------------------------------------------------+
| Grants for youenn@localhost                                                                                   |
+---------------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO 'youenn'@'localhost' IDENTIFIED BY PASSWORD '*2470C0C06DEE42FD1618BB99005ADCA2EC9D1E19' |
| GRANT ALL PRIVILEGES ON `mabase`.* TO 'youenn'@'localhost'                                                    |
+---------------------------------------------------------------------------------------------------------------+
2 rows in set (0.00 sec)

````
- Pour que les privilèges prennent effet directement on tape :
````
MariaDB [mabase]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.00 sec)
````

- On va ensuite ouvrir un port firewall pour que d'autres machines puissent accéder à la base, on regarde quel port est utilisé par mysql(ici 3306) :

````
[youenn@db ~]$ sudo ss -alpnt
LISTEN     0      50                               *:3306                                         *:*                   users:(("mysqld",pid=2603,fd=14))
````

- On ouvre donc le port sur le pare feu :

 ```` 
[youenn@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
[sudo] password for youenn:
Success
````

- On reload le pare feu : 
````
[youenn@web ~]$ sudo firewall-cmd –reload
````

- On voit que notre base fonctionne correctement :
````
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mabase             |
| mysql              |
| performance_schema |
| test               |
+--------------------+
````

## II.	Serveur Web

- On installe le serveur web : 
````
[youenn@web ~]$ sudo yum install -y httpd
````

````
On installe php :
[youenn@web ~]$ sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y
````

- Pour télécharger wordpress, on install wget : 
````
[youenn@web ~]$ sudo yum install wget
````
- On va chercher le paquet : 
````
[youenn@web ~]$ wget https://wordpress.org/latest.tar.gz
````
 - On le décompresse : 
````
[youenn@web ~]$ tar xzvf latest.tar.gz
wordpress/
wordpress/xmlrpc.php
wordpress/wp-blog-header.php
[…]
````
- On extrait l’archive wordpress dans un dossier : 
````
[youenn@web ~]$ sudo yum install rsync
[youenn@web ~]$ sudo rsync -avP wordpress/ /var/www/html/
````

- On crée un dossier uploads pour que wordpress stocke les fichiers téléchargés :
````
[youenn@web ~]$ cd /var/www/html/wp-content/
[youenn@web wp-content]$ mkdir uploads
[youenn@web wp-content]$ ls
index.php  plugins  themes  uploads
````
- On applique des droits pour Wordpress :
````
[youenn@web ~]$ sudo chown -R apache:apache /var/www/html/*
````

- On va renseigner à Wordpress les informations qu'il utilisera pour se connecter à la base de données, pour cela on renseignera dans le fichier conf que l’ont aura crée :
````
[youenn@web html]$ sudo cp wp-config-sample.php wp-config.php
[youenn@web html]$ sudo vim wp-config.php
[youenn@web html]$ sudo chown apache:apache wp-config.php
````
- On y ajoute :
````
define( 'DB_NAME', 'mabase' );

/** MySQL database username */
define( 'DB_USER', 'youenn' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', '10.99.99.12' );
````
- Ensuite on va lancer le serveur de base de donnée, puis le serveur web : 
````
[youenn@db ~]$ systemctl status mariadb
● mariadb.service - MariaDB database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-01-07 11:22:22 CET; 4h 12min ago

[youenn@web html]$ systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-01-07 15:36:27 CET; 3s ago
````
- On ouvre le port, pour cela :
````
[youenn@web html]$ sudo ss -alpnt
LISTEN     0      128                           [::]:80                                        [::]:*                   users:(("httpd",pid=2714,fd=4),("httpd",pid=2713,fd=4),("httpd",pid=2712,fd=4),("httpd",pid=2711,fd=4),("httpd",pid=2710,fd=4),("httpd",pid=2708,fd=4))

[youenn@web html]$ sudo firewall-cmd --add-port=80/tcp --permanent 
````

- On oublie pas de reload le pare feu 
````
[youenn@web ~]$ sudo firewall-cmd –reload
````
On vérifie que l’on peux bien accéder à notre interface web.



## III.	Reverse Proxy
- On va installer un serveur NGINX 
````
[youenn@rp ~]$ sudo yum install epel-release
[youenn@rp ~]$ sudo yum install nginx -y
````
- On ouvre le port 80 :
````
[youenn@rp ~]$ sudo firewall-cmd --add-port=80/tcp –permanent
````
- On oublie pas de reload : 
````
[youenn@rp nginx]$ sudo firewall-cmd --reload
````
- ON va modifier notre fichier conf :
````
[youenn@rp nginx]$ sudo vim nginx.conf
````
- On supprime tous et on y mets :
````
events {}

http {
    server {
        listen       80;

        server_name web.tp2.cesi;

        location / {
            proxy_pass   http://10.99.99.11:80;
        }
    }
}
````
- On va configurer ensuite le firewall de nginx, pour cela on ouvre le port 80 sur autre machines.
